package com.example.nerexireyes.estudiantespruebas.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.nerexireyes.estudiantespruebas.entities.estudiantesDB;

public class estudianteElemenHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "EstudiantesElements.db";

    public estudianteElemenHelper( Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(estudiantesDB.EstudiantesElementEntry.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(estudiantesDB.EstudiantesElementEntry.DELETE_TABLE);
        onCreate(db);
    }
}
