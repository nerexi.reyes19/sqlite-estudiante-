package com.example.nerexireyes.estudiantespruebas.entities;

import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.example.nerexireyes.estudiantespruebas.helper.estudianteElemenHelper;
import com.example.nerexireyes.estudiantespruebas.model.estudiantesItem;

import java.util.ArrayList;

public class estudiantesDB {
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private estudianteElemenHelper dbHelper;

    public estudiantesDB(Context context) {
        // Create new helper
        dbHelper = new estudianteElemenHelper(context);
    }
    public static abstract class EstudiantesElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        ContentValues contentValues = new ContentValues();
        contentValues.put(EstudiantesElementEntry.COLUMN_NAME_TITLE, productName);
        dbHelper.getWritableDatabase().insert(EstudiantesElementEntry.TABLE_NAME, null, contentValues);
    }
    public ArrayList<estudiantesItem> getAllItems() {

        ArrayList<estudiantesItem> estudiantesItems = new ArrayList<>();

        String[] allColumns = { EstudiantesElementEntry._ID,
                EstudiantesElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
                EstudiantesElementEntry.TABLE_NAME,    // The table to query
                allColumns,                         // The columns to return
                null,                               // The columns for the WHERE clause
                null,                               // The values for the WHERE clause
                null,                               // don't group the rows
                null,                               // don't filter by row groups
                null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            estudiantesItem estudiantesItem = new estudiantesItem(getItemId(cursor), getItemName(cursor));
            estudiantesItems.add(estudiantesItem);
            cursor.moveToNext();
        }
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return estudiantesItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(EstudiantesElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(EstudiantesElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        //sobreescribe la base de datos y elimina los datos de la tabla
        dbHelper.getWritableDatabase().delete(EstudiantesElementEntry.TABLE_NAME, null, null);

    }

    public void updateItem(estudiantesItem estudiantesItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        ContentValues contentValues = new ContentValues();
        contentValues.put(EstudiantesElementEntry.COLUMN_NAME_TITLE, estudiantesItem.getName());
        dbHelper.getWritableDatabase().update(EstudiantesElementEntry.TABLE_NAME, contentValues,
                EstudiantesElementEntry._ID+ "="+ estudiantesItem.getId(),null);
    }

    public void deleteItem(estudiantesItem shoppingItem) {
        String [] lugar = {shoppingItem.getName()};
        dbHelper.getWritableDatabase().delete(EstudiantesElementEntry.TABLE_NAME,
                EstudiantesElementEntry.COLUMN_NAME_TITLE + " LIKE ?", lugar);
    }
}

