package com.example.nerexireyes.estudiantespruebas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.nerexireyes.estudiantespruebas.R;
import com.example.nerexireyes.estudiantespruebas.model.estudiantesItem;

import java.util.ArrayList;

public class EstudianteItemAdapter extends ArrayAdapter<estudiantesItem> {

    private Context context;
    private ArrayList<estudiantesItem> estudiantesItems;
    public EstudianteItemAdapter(Context context, ArrayList<estudiantesItem> estudiantesItems) {
        super(context, R.layout.activity_mostrar);
        this.context = context;
        this.estudiantesItems = estudiantesItems;
    }
    @Override
    public int getCount() {
        return estudiantesItems.size();
    }

    @Override
    public estudiantesItem getItem(int position) {
        return  estudiantesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return estudiantesItems.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder viewHolder;

        if (convertView == null || convertView.getTag() == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.activity_mostrar, parent, false);
            viewHolder.mItemName = view.findViewById(R.id.estudiante_item);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            view = convertView;
        }
        // Set text with the item name
        viewHolder.mItemName.setText(estudiantesItems.get(position).getName());

        return view;
    }

    static class ViewHolder {
        TextView mItemName;
    }
}


